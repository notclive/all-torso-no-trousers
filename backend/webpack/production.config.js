const path = require('path');
const config = require('./base.config');

config.mode = 'production';
config.entry = './src/lambda.ts';
config.output.filename = 'lambda.js';
config.resolve.alias = {
    [path.resolve(__dirname, '../src/database/client/development.config.ts')]:
        path.resolve(__dirname, '../src/database/client/production.config.ts')
};

module.exports = config;