const path = require('path');
const webpack = require('webpack');

module.exports = {
    module: {
        rules: [
            {
                test: /\.ts/,
                use: 'ts-loader'
            }
        ]
    },
    output: {
        libraryTarget: 'commonjs',
        path: path.join(__dirname, '../build')
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    plugins: [
        // lambda-api depends on aws-sdk but we don't make use of it.
        // aws-sdk is many MB in size slowing down build and deployment.
        new webpack.NormalModuleReplacementPlugin(
            /^aws-sdk$/,
            path.join(__dirname, 'dummy-aws-sdk.ts')
        )
    ],
    target: 'node'
}