const NodemonPlugin = require('nodemon-webpack-plugin');
const config = require('./base.config');

config.mode = 'development';
config.entry = './src/local.ts';
config.output.filename = 'local.js';
config.plugins.push(new NodemonPlugin());

module.exports = config;