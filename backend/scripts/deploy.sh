cd build
rm -f lambda.zip
zip lambda.zip lambda.js
aws lambda update-function-code \
  --region eu-west-2 \
  --function-name all-torso-no-trousers-backend \
  --zip-file fileb://./lambda.zip