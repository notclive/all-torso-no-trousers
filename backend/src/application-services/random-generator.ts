import {nanoid} from 'nanoid'

export function generateSecret() {
    return nanoid(12);
}

export function generateId() {
    return nanoid(9);
}