import {APIGatewayEvent, Context} from 'aws-lambda';
import {createServer, IncomingMessage} from 'http';
import {AddressInfo} from 'net';
import {URL} from 'url';
import handler from './handler';

function createEvent(request: IncomingMessage, url: URL, body: string) {
    // The event object we're faking is a lightweight based on:
    // https://docs.aws.amazon.com/lambda/latest/dg/eventsources.html#eventsources-api-gateway-request
    return {
        httpMethod: request.method!.toUpperCase(),
        path: url.pathname,
        resource: '/{proxy+}',
        queryStringParameters: [...url.searchParams.keys()].reduce((output, key) => {
            output[key] = url.searchParams.get(key);
            return output;
        }, {}),
        headers: request.headers,
        requestContext: {},
        pathParameters: {},
        stageVariables: {},
        isBase64Encoded: false,
        body: body,
    } as APIGatewayEvent;
}

const server = createServer((request, response) => {

    const url = new URL(request.url!, `http://${request.headers.host}/`);
    let body = '';

    request.on('data', chunk => {
        body += chunk;
    });

    request.on('end', () => {
        const event = createEvent(request, url, body);
        handler.run(event, {} as Context)
            .then((handlerResponse) => {
                let {
                    body,
                    headers,
                    statusCode,
                } = handlerResponse;

                if (handlerResponse.isBase64Encoded) {
                    body = Buffer.from(body, 'base64');
                }

                if (!headers['content-length'] && body) {
                    headers['content-length'] = body.length;
                }

                response.writeHead(statusCode, headers);
                response.end(body);
            })
            .catch((err) => {
                console.error(err);
                response.writeHead(500, {'content-length': 0});
                response.end('');
            });
    });
});

server.listen(process.env.PORT || 5000, () => {
    console.log(`Listening on port ${(server.address() as AddressInfo).port}`);
});
