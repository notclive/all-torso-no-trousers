import handler from './handler';
import {traceWithXRay} from './monitoring/xray-middleware';

export async function run(event: any, context: any) {

    // Temporary workaround until lambda-api 0.11 is released.
    if (event.version === '2.0') {
        event.httpMethod = event.requestContext.http.method;
        event.path = event.requestContext.http.path;
    }

    return await traceWithXRay(event, context, (e, c) => handler.run(e, c));
}
