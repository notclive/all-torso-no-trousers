import {Group} from '../database/models/group';
import {Member} from '../database/models/member';

export function areResultsReleased(group: Group | null, members: Member[]) {
    return group?.ResultsReleasedEarly || members.every(m => !!m.MemberIdsFromTallestToShortest);
}