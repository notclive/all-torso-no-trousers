import lambdaApi from 'lambda-api'
import {createGroup} from './commands/create-group';
import {releaseResultsNow} from './commands/release-results-now';
import {submitGuess} from './commands/submit-guess';
import {submitHeight} from './commands/submit-height';
import {getGroup} from './queries/get-group';
import {getGroupForAdministration} from './queries/get-group-for-administration';
import {getSelf} from './queries/get-self';

const framework = lambdaApi({
    logger: true,
    errorHeaderWhitelist: [
        'access-control-allow-origin',
        'access-control-allow-methods',
        'access-control-allow-headers',
    ]
});

framework.use((_, response, next) => {
    response.cors({
        origin: '*',
        methods: '*',
        headers: '*'
    });
    next();
});

framework.options('/*', (_, response) => {
    response.send({});
});

framework.post('/groups', createGroup);
framework.get('/groups/:groupId/administration', getGroupForAdministration);
framework.post('/groups/:groupId/release-results-now', releaseResultsNow);

framework.get('/groups/:groupId', getGroup);
framework.get('/groups/:groupId/members/:memberId', getSelf);
framework.put('/groups/:groupId/members/:memberId/height', submitHeight);
framework.put('/groups/:groupId/members/:memberId/guess', submitGuess);

export default framework;