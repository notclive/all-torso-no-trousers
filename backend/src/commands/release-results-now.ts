import {Request, Response} from 'lambda-api';
import {Member} from '../database/models/member';
import {fetchGroup, persistGroup} from '../database/repositories/group-repository';
import {fetchMembersInGroup, persistMember} from '../database/repositories/member-repository';

function removeFromGuess(memberWithGuess: Member, idsToRemove: string[]) {
    memberWithGuess.MemberIdsFromTallestToShortest
        = memberWithGuess.MemberIdsFromTallestToShortest?.filter(
        guessedId => !idsToRemove.includes(guessedId)
    );
    return persistMember(memberWithGuess);
}

export async function releaseResultsNow(request: Request, response: Response) {

    const groupId = request.params.groupId!;
    const group = await fetchGroup(groupId);
    if (!group) {
        return response.error(404, `There is no group with ID '${groupId}'`);
    }

    if (group.Secret !== request.headers.authorization) {
        return response.sendStatus(403);
    }

    const members = await fetchMembersInGroup(group.Id);
    const idsOfMembersWithoutHeight = members.filter(m => !m.HeightInCm).map(m => m.Id);
    await Promise.all([
        ...members.map(m => removeFromGuess(m, idsOfMembersWithoutHeight))
    ]);

    group.ResultsReleasedEarly = true;
    await persistGroup(group);

    response.sendStatus(204);
}