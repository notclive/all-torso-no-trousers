import {Request, Response} from 'lambda-api';
import {generateId, generateSecret} from '../application-services/random-generator';
import {Group} from '../database/models/group';
import {persistGroup} from '../database/repositories/group-repository';
import {persistMember} from '../database/repositories/member-repository';

export interface CreateGroupRequest {
    groupName: string;
    memberNames: string[];
}

export interface GroupCreatedResponse {
    groupId: string;
    secret: string;
}

function createNewGroup(groupName: string) {
    return {
        Id: generateId(),
        Secret: generateSecret(),
        Name: groupName
    };
}

function createNewMember(group: Group, name: string) {
    return {
        Id: generateId(),
        Secret: generateSecret(),
        GroupId: group.Id,
        Name: name
    };
}

function createResponse(group: Group): GroupCreatedResponse {
    return {
        groupId: group.Id,
        secret: group.Secret
    };
}

export async function createGroup(request: Request, response: Response) {

    const body = request.body as CreateGroupRequest;
    const group = createNewGroup(body.groupName);

    await Promise.all([
        ...body.memberNames
            .map(name => createNewMember(group, name))
            .map(member => persistMember(member)),
        persistGroup(group)
    ]);

    response.json(createResponse(group));
}