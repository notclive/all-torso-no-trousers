import {Request, Response} from 'lambda-api';
import {fetchMember, persistMember} from '../database/repositories/member-repository';

export interface SubmitHeightRequest {
    heightInCm: number;
}

export async function submitHeight(request: Request, response: Response) {

    const body = request.body as SubmitHeightRequest;
    const groupId = request.params.groupId!;
    const memberId = request.params.memberId!;

    const member = await fetchMember(groupId, memberId);
    if (!member) {
        return response.error(404, `There is no member with ID '${memberId}' in group with ID '${groupId}'`);
    }

    if (member.Secret !== request.headers.authorization) {
        return response.sendStatus(403);
    }

    member.HeightInCm = body.heightInCm;
    await persistMember(member);

    response.sendStatus(204);
}