import {Request, Response} from 'lambda-api';
import {Member} from '../database/models/member';
import {fetchGroup} from '../database/repositories/group-repository';
import {fetchMember, fetchMembersInGroup, persistMember} from '../database/repositories/member-repository';
import {areResultsReleased} from '../domain-services/are-results-released';

export interface SubmitGuessRequest {
    memberIdsFromTallestToShortest: string[];
}

function haveAllMembersBeenProvided(request: SubmitGuessRequest, groupMembers: Member[]) {
    const providedMembers = request.memberIdsFromTallestToShortest;
    const expectedMembers = groupMembers.map(m => m.Id);
    return providedMembers.length === expectedMembers.length
        && expectedMembers.every(m => providedMembers.indexOf(m) >= 0);
}

export async function submitGuess(request: Request, response: Response) {

    const body = request.body as SubmitGuessRequest;
    const groupId = request.params.groupId!;
    const memberId = request.params.memberId!;

    const member = await fetchMember(groupId, memberId);
    if (!member) {
        return response.error(404, `There is no member with ID '${memberId}' in group with ID '${groupId}'`);
    }

    if (member.Secret !== request.headers.authorization) {
        return response.sendStatus(403);
    }

    const group = await fetchGroup(groupId);
    const members = await fetchMembersInGroup(groupId);

    if (areResultsReleased(group, members)) {
        return response.error(403, 'Results have been released, it is too late to guess');
    }

    if (!haveAllMembersBeenProvided(body, members))
    {
        return response.error(400, 'Not all members of the group were included in the guess');
    }

    member.MemberIdsFromTallestToShortest = body.memberIdsFromTallestToShortest;
    await persistMember(member);

    response.sendStatus(204);
}
