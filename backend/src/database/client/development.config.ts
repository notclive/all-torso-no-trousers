export default {
    endpoint: 'http://localhost:8000',
    region: 'eu-west-2',
    credentials: {
        accessKeyId: 'not-needed-for-local-dynamodb',
        secretAccessKey: 'not-needed-for-local-dynamodb'
    }
};