import {DynamoDBClient} from '@aws-sdk/client-dynamodb';
// Webpack replaces this with the production config, when making the production build.
import config from './development.config';

export default new DynamoDBClient(config);
