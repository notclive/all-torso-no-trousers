import {GetItemCommand, PutItemCommand, QueryCommand} from '@aws-sdk/client-dynamodb';
import {marshall, unmarshall} from '@aws-sdk/util-dynamodb';
import client from '../client/dynamo-db-client';
import {Member} from '../models/member';

export function persistMember(member: Member) {
    return client.send(new PutItemCommand({
        TableName: 'Member',
        Item: marshall(member, {removeUndefinedValues: true}),
    }));
}

export function fetchMember(groupId: string, memberId: string) {
    return client.send(new GetItemCommand({
        TableName: 'Member',
        Key: {
            GroupId: {'S': groupId},
            Id: {'S': memberId}
        }
    })).then(response => {
        return response.Item
            ? unmarshall(response.Item) as Member
            : null;
    });
}

export function fetchMembersInGroup(groupId: string) {
    return client.send(new QueryCommand({
        TableName: 'Member',
        KeyConditionExpression: 'GroupId = :groupId',
        ExpressionAttributeValues: {
            ':groupId': {S: groupId}
        }
    })).then(response => {
        return response.Items
            ? response.Items.map(i => unmarshall(i) as Member)
            : [];
    });
}