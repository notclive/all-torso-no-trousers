import {GetItemCommand, PutItemCommand} from '@aws-sdk/client-dynamodb';
import {marshall, unmarshall} from '@aws-sdk/util-dynamodb';
import client from '../client/dynamo-db-client';
import {Group} from '../models/group';

export function persistGroup(group: Group) {
    return client.send(new PutItemCommand({
        TableName: 'Group',
        Item: marshall(group, {removeUndefinedValues: true})
    }));
}

export async function fetchGroup(groupId: string) {
    return client.send(new GetItemCommand({
        TableName: 'Group',
        Key: {
            Id: {'S': groupId}
        }
    })).then(response => {
        return response.Item
            ? unmarshall(response.Item) as Group
            : null;
    });
}
