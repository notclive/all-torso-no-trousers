export interface Group {
    Id: string;
    Secret: string;
    Name: string;
    ResultsReleasedEarly?: boolean;
}