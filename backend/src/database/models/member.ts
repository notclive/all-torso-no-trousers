export interface Member {
    Id: string;
    Secret: string;
    GroupId: string;
    Name: string;
    HeightInCm?: number;
    MemberIdsFromTallestToShortest?: string[];
}