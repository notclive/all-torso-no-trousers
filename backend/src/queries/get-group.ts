import {Request, Response} from 'lambda-api';
import {Group} from '../database/models/group';
import {Member} from '../database/models/member';
import {fetchGroup} from '../database/repositories/group-repository';
import {fetchMembersInGroup} from '../database/repositories/member-repository';
import {areResultsReleased} from '../domain-services/are-results-released';

export interface GroupResponse {
    name: string;
    members: ArrangeableMemberResponse[];
    results?: ResultsResponse;
}

export interface ArrangeableMemberResponse {
    id: string;
    name: string;
    hasGuessed: boolean;
}

export interface ResultsResponse {
    actualHeightsFromTallestToShortest: ActualHeight[];
    bestGuesser: BestGuesserResponse;
    biggestOverestimateOfOwnHeight: BiggestOverestimateOfOwnHeightResponse;
    mostOverestimated: MostOverestimatedResponse;
    mostUnderestimated: MostUnderestimatedResponse;
}

export interface ActualHeight {
    memberId: string;
    name: string;
    heightInCm: number;
}

export interface BestGuesserResponse {
    memberId: string;
    positionsOff: number;
}

export interface BiggestOverestimateOfOwnHeightResponse {
    memberId: string;
    positionsOff: number;
}

export interface MostOverestimatedResponse {
    memberId: string;
    averagePlacement: number;
}

export interface MostUnderestimatedResponse {
    memberId: string;
    averagePlacement: number;
}

export async function getGroup(request: Request, response: Response) {

    const groupId = request.params.groupId!;
    const group = await fetchGroup(groupId);
    if (!group) {
        return response.error(404, `There is no group with ID '${groupId}'`);
    }

    const members = await fetchMembersInGroup(group.Id);
    const caller = members.find(m => m.Secret === request.headers.authorization!);
    if (!caller) {
        return response.sendStatus(403);
    }

    response.json(createResponse(group, members));
}

function createResponse(group: Group, members: Member[]): GroupResponse {
    const memberResponses = members.map(m => ({
        id: m.Id,
        name: m.Name,
        hasGuessed: !!m.MemberIdsFromTallestToShortest?.length
    }));
    return {
        name: group.Name,
        members: memberResponses,
        results: areResultsReleased(group, members) ? calculateResults(members) : undefined
    };
}

function calculateResults(members: Member[]) {

    const membersWitHeights = members.filter(m => m.HeightInCm);
    const membersWithGuesses = members.filter(m => m.MemberIdsFromTallestToShortest);
    const actualHeightsInOrder = calculateActualHeightsInOrder();
    const averagePlacements = calculateAveragePlacements();

    return {
        actualHeightsFromTallestToShortest: actualHeightsInOrder,
        bestGuesser: determineBestGuesser(),
        biggestOverestimateOfOwnHeight: determineBiggestOverestimateOfOwnHeight(),
        mostOverestimated: determineMostOverestimated(),
        mostUnderestimated: determineMostUnderestimated()
    };

    function calculateActualHeightsInOrder() {
        return membersWitHeights
            .map(m => ({memberId: m.Id, name: m.Name, heightInCm: m.HeightInCm!}))
            .sort(descending(m => m.heightInCm));
    }

    function calculateAveragePlacements() {
        return Object.assign({}, ...members.map(m => ({
            [m.Id]: calculateAveragePlacement(m.Id)
        })));
    }

    function calculateAveragePlacement(memberId: string) {
        const sum = membersWithGuesses
            .map(m => m.MemberIdsFromTallestToShortest!.indexOf(memberId) + 1)
            .reduce((a, b) => a + b);
        return (sum / membersWithGuesses.length).toFixed(1);
    }

    function determineBestGuesser() {
        return membersWithGuesses.map(m => ({
                memberId: m.Id,
                positionsOff: sumOfPositionsFromActual(m.MemberIdsFromTallestToShortest!)
            }))
            .sort(ascending(m => m.positionsOff))
            .shift()!;
    }

    function sumOfPositionsFromActual(guess: string[]) {
        return guess
            .map((memberId, index) => Math.abs(findActualPosition(memberId) - (index + 1)))
            .reduce((a, b) => a + b);
    }

    function determineBiggestOverestimateOfOwnHeight() {
        return membersWithGuesses.map(member => {
                const actualPosition = findActualPosition(member.Id);
                const placementOfSelf = member.MemberIdsFromTallestToShortest!.indexOf(member.Id) + 1;
                return {
                    memberId: member.Id,
                    positionsOff: actualPosition - placementOfSelf
                }
            })
            .sort(descending(m => m.positionsOff))
            .shift()!;
    }

    function determineMostOverestimated() {
        return members.map(member => ({
                memberId: member.Id,
                averagePlacement: averagePlacements[member.Id]
            }))
            .sort(descending(m => findActualPosition(m.memberId) - m.averagePlacement))
            .shift()!;
    }

    function determineMostUnderestimated() {
        return members.map(member => ({
                memberId: member.Id,
                averagePlacement: averagePlacements[member.Id]
            }))
            .sort(descending(m => m.averagePlacement - findActualPosition(m.memberId)))
            .shift()!;
    }

    function findActualPosition(memberId: string) {
        return actualHeightsInOrder.findIndex(m => m.memberId === memberId) + 1;
    }

    function ascending<T>(propertyExtractor: (itemToSort: T) => number) {
        return (a: T, b: T) => propertyExtractor(a) - propertyExtractor(b);
    }

    function descending<T>(propertyToSortWith: (toSort: T) => number) {
        return (a: T, b: T) => propertyToSortWith(b) - propertyToSortWith(a);
    }
}