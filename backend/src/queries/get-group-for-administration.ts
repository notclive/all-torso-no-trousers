import {Request, Response} from 'lambda-api';
import {Group} from '../database/models/group';
import {Member} from '../database/models/member';
import {fetchGroup} from '../database/repositories/group-repository';
import {fetchMembersInGroup} from '../database/repositories/member-repository';
import {areResultsReleased} from '../domain-services/are-results-released';

export interface GroupForAdministrationResponse {
    name: string;
    members: MemberForAdministrationResponse[];
    resultsAreReleased: boolean
}

export interface MemberForAdministrationResponse {
    id: string;
    name: string;
    secret: string;
    hasSubmittedHeight: boolean;
    hasGuessed: boolean;
}

function createResponse(group: Group, members: Member[]): GroupForAdministrationResponse {
    return {
        name: group.Name,
        resultsAreReleased: areResultsReleased(group, members),
        members: members.map(m => ({
            id: m.Id,
            name: m.Name,
            secret: m.Secret,
            hasSubmittedHeight: !!m.HeightInCm,
            hasGuessed: !!m.MemberIdsFromTallestToShortest
        }))
    };
}

export async function getGroupForAdministration(request: Request, response: Response) {

    const groupId = request.params.groupId!;
    const group = await fetchGroup(groupId);
    if (!group) {
        return response.error(404, `There is no group with ID '${groupId}'`);
    }

    if (group.Secret !== request.headers.authorization) {
        return response.sendStatus(403);
    }

    const members = await fetchMembersInGroup(group.Id);
    response.json(createResponse(group, members));
}