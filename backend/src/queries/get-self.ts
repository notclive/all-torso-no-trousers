import {Request, Response} from 'lambda-api';
import {Member} from '../database/models/member';
import {fetchMember} from '../database/repositories/member-repository';

export interface MemberForSelfResponse {
    heightInCm?: number;
    memberIdsFromTallestToShortest?: string[];
}

function createResponse(member: Member): MemberForSelfResponse {
    return {
        heightInCm: member.HeightInCm,
        memberIdsFromTallestToShortest: member.MemberIdsFromTallestToShortest
    };
}

export async function getSelf(request: Request, response: Response) {

    const groupId = request.params.groupId!;
    const memberId = request.params.memberId!;
    const member = await fetchMember(groupId, memberId);
    if (!member) {
        return response.error(404, `There is no member with ID '${memberId}' in group with ID '${groupId}'`);
    }

    if (member.Secret !== request.headers.authorization) {
        return response.sendStatus(403);
    }

    response.json(createResponse(member));
}