import {APIGatewayEvent, APIGatewayProxyResult, Context} from 'aws-lambda';
import xray, {Segment} from 'aws-xray-sdk-core';
import {IncomingRequestData} from 'aws-xray-sdk-core/lib/middleware/mw_utils';
import {IncomingMessage, ServerResponse} from 'http';

// We don't rely on the built in tracing, it doesn't capture the HTTP request method and path.
// Because this isn't yet captured at the HTTP Gateway level.
export async function traceWithXRay(
    event: APIGatewayEvent,
    context: Context,
    next: (event: APIGatewayEvent, context: Context) => Promise<APIGatewayProxyResult>
) {
    const replacement = replaceImmutableSegment(context, event);
    const result = await next(event, context);
    endSegment(replacement, result)
    return result;
}

// X-Ray automatically creates an immutable segment for each lambda invocation.
// Replace it so that we can add our own segment data.
function replaceImmutableSegment(context: Context, event: APIGatewayEvent) {
    const traceIds = extractTraceIds();
    const replacementSegment = new Segment(
        `${context.functionName} handler`,
        traceIds.rootId,
        traceIds.parentId
    );
    const requestDataForXRay = new IncomingRequestData({
        method: event.httpMethod,
        url: event.path,
        headers: event.headers,
        connection: {
            secure: event.headers['x-forwarded-proto'] === 'https'
        }
    } as unknown as IncomingMessage);
    replacementSegment.addIncomingRequestData(requestDataForXRay);
    xray.setSegment(replacementSegment);
    return {replacementSegment, requestDataForXRay};
}

function extractTraceIds() {
    const tracingInfo = process.env._X_AMZN_TRACE_ID || '';
    const matches = tracingInfo.match(/^Root=(.+);Parent=(.+);/) || ['', '', ''];
    return {rootId: matches[1], parentId: matches[2]};
}

function endSegment(
    {replacementSegment, requestDataForXRay}: {replacementSegment: Segment, requestDataForXRay: IncomingRequestData},
    result: APIGatewayProxyResult
) {
    requestDataForXRay.close({
        statusCode: result.statusCode,
        headers: result.headers
    } as unknown as ServerResponse);
    // In X-Ray terminology a fault is a 5XX.
    if (result.statusCode >= 500) {
        replacementSegment.addError(result.body);
        replacementSegment.addFaultFlag();
    // In X-Ray terminology an error is a 4XX.
    } else if (result.statusCode >= 400) {
        replacementSegment.addError(result.body);
        replacementSegment.addErrorFlag();
        // The SDK incorrectly sets the fault flag on add error.
        // See https://github.com/aws/aws-xray-sdk-node/issues/61.
        replacementSegment['fault'] = false;
    }
    replacementSegment.close();
}
