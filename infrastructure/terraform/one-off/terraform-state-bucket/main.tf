terraform {
  required_version = "0.14.9"
}

provider "aws" {
  region = "eu-west-2"
}

resource "aws_s3_bucket" "terraform_state" {

  bucket = "all-torso-no-trousers-terraform"
  acl    = "private"

  tags = {
    project = "all-torso-no-trousers"
  }
}

resource "aws_s3_bucket_public_access_block" "terraform_state" {
  bucket                  = aws_s3_bucket.terraform_state.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
