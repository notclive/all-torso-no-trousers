terraform {
  required_version = "0.14.9"
}

provider "aws" {
  region = "eu-west-2"
}

resource "aws_iam_user" "gitlab" {

  name = "all-torso-no-trousers-gitlab"

  tags = {
    project = "all-torso-no-trousers"
  }
}

resource "aws_iam_user_policy_attachment" "s3_full_access" {
  user       = aws_iam_user.gitlab.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_user_policy_attachment" "dynamo_db_full_access" {
  user       = aws_iam_user.gitlab.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_user_policy_attachment" "lambda_full_access" {
  user       = aws_iam_user.gitlab.name
  policy_arn = "arn:aws:iam::aws:policy/AWSLambda_FullAccess"
}

resource "aws_iam_user_policy_attachment" "ecr_full_access" {
  user       = aws_iam_user.gitlab.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess"
}

resource "aws_iam_user_policy_attachment" "cloud_watch_full_access" {
  user       = aws_iam_user.gitlab.name
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}

resource "aws_iam_user_policy_attachment" "api_gateway_full_access" {
  user       = aws_iam_user.gitlab.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonAPIGatewayAdministrator"
}

resource "aws_iam_user_policy_attachment" "iam_full_access" {
  user       = aws_iam_user.gitlab.name
  policy_arn = "arn:aws:iam::aws:policy/IAMFullAccess"
}

resource "aws_iam_user_policy_attachment" "acm_full_access" {
  user       = aws_iam_user.gitlab.name
  policy_arn = "arn:aws:iam::aws:policy/AWSCertificateManagerFullAccess"
}

resource "aws_iam_user_policy_attachment" "route_53_full_access" {
  user       = aws_iam_user.gitlab.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonRoute53FullAccess"
}

resource "aws_iam_user_policy_attachment" "clloud_front_full_access" {
  user       = aws_iam_user.gitlab.name
  policy_arn = "arn:aws:iam::aws:policy/CloudFrontFullAccess"
}
