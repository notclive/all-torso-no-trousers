terraform {
  required_version = "0.14.9"
  
  # This bucket is created by the Terraform script in one-off/terraform-state-bucket/main.tf.
  backend "s3" {
    bucket   = "all-torso-no-trousers-terraform"
    region   = "eu-west-2"
    key      = "terraform.tfstate"
    encrypt  = "true"
  }
}

provider "aws" {
  region = "eu-west-2"
}

provider "aws" {
  region = "us-east-1"
  alias = "us_east_1"
}
