resource "aws_ecr_repository" "backend" {

  name = "all-torso-no-trousers-backend"
  image_scanning_configuration {
    scan_on_push = true
  }

  tags = {
    project = "all-torso-no-trousers"
  }
}

data "aws_iam_policy_document" "backend_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "backend" {

  name               = "all-torso-no-trousers-backend"
  description        = "Role that executes the all-torso-no-trousers backend"
  assume_role_policy = data.aws_iam_policy_document.backend_assume_role.json

  tags = {
    project = "all-torso-no-trousers"
  }
}

resource "aws_iam_role_policy_attachment" "backend_dynamo_db" {
  role       = aws_iam_role.backend.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess"
}

resource "aws_iam_role_policy_attachment" "backend_lambda_execution" {
  role       = aws_iam_role.backend.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "backend_xray" {
  role       = aws_iam_role.backend.name
  policy_arn = "arn:aws:iam::aws:policy/AWSXRayDaemonWriteAccess"
}

resource "aws_cloudwatch_log_group" "backend_lambda" {

  name              = "/aws/lambda/all-torso-no-trousers-backend"
  retention_in_days = 7

  tags = {
    project = "all-torso-no-trousers"
  }
}

data "archive_file" "dummy_backend" {

  type        = "zip"
  output_path = "dummy-lambda.zip"

  source {
    content  = "dummy-file"
    filename = "lambda.js"
  }
}

resource "aws_lambda_function" "backend" {

  function_name = "all-torso-no-trousers-backend"
  role          = aws_iam_role.backend.arn
  timeout       = 10
  runtime       = "nodejs14.x"
  handler       = "lambda.run"
  filename      = data.archive_file.dummy_backend.output_path
  # Hash of file is intentionally not provided so that the handler won't be replaced when provisioning.
  # Deployment is performed in a separate script.

  tracing_config {
    mode = "Active"
  }

  depends_on = [
    aws_cloudwatch_log_group.backend_lambda
  ]

  tags = {
    project = "all-torso-no-trousers"
  }
}

resource "aws_apigatewayv2_api" "backend" {

  name          = "all-torso-no-trousers-backend"
  protocol_type = "HTTP"
  target        = aws_lambda_function.backend.arn

  tags = {
    project = "all-torso-no-trousers"
  }
}

resource "aws_lambda_permission" "permit_gateway_to_invoke_lambda" {
  statement_id  = "AllTorsoNoTrousers-PermitGatewayToInvokeLambda"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.backend.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.backend.execution_arn}/*/*"
}
