resource "aws_route53_zone" "domain" {
  name = "alltorsonotrousers.com"

  tags = {
    project = "all-torso-no-trousers"
  }
}

resource "aws_acm_certificate" "domain" {

  domain_name               = "alltorsonotrousers.com"
  validation_method         = "DNS"
  subject_alternative_names = ["*.alltorsonotrousers.com"]

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    project = "all-torso-no-trousers"
  }
}

resource "aws_route53_record" "certificate_validation" {
  for_each = {
    for dvo in aws_acm_certificate.domain.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = aws_route53_zone.domain.zone_id
}

resource "aws_acm_certificate_validation" "domain" {
  certificate_arn         = aws_acm_certificate.domain.arn
  validation_record_fqdns = [for record in aws_route53_record.certificate_validation : record.fqdn]
}
