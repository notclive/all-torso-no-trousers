resource "aws_dynamodb_table" "member" {

  name           = "Member"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "GroupId"
  range_key      = "Id"

  attribute {
    name = "GroupId"
    type = "S"
  }
  
  attribute {
    name = "Id"
    type = "S"
  }

  tags = {
    project = "all-torso-no-trousers"
  }
}

resource "aws_dynamodb_table" "group" {

  name           = "Group"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "Id"

  attribute {
    name = "Id"
    type = "S"
  }

  tags = {
    project = "all-torso-no-trousers"
  }
}
