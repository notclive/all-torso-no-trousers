resource "aws_s3_bucket" "frontend" {

  bucket = "all-torso-no-trousers-frontend"
  acl    = "private"

  tags = {
    project = "all-torso-no-trousers"
  }
}

data "aws_iam_policy_document" "frontend_public" {
  statement {
    actions   = ["s3:List*", "s3:GetObject"]
    resources = [aws_s3_bucket.frontend.arn, "${aws_s3_bucket.frontend.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}

resource "aws_s3_bucket_policy" "frontend_public" {
  bucket = aws_s3_bucket.frontend.id
  policy = data.aws_iam_policy_document.frontend_public.json
}

resource "aws_acm_certificate" "frontend" {

  # Cloudfront requires its certificates to be hosted in us-east-1.
  provider          = aws.us_east_1
  domain_name       = "alltorsonotrousers.com"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    project = "all-torso-no-trousers"
  }
}

resource "aws_cloudfront_distribution" "frontend" {

  enabled             = true
  aliases             = ["alltorsonotrousers.com"]
  default_root_object = "index.html"

  origin {
    domain_name = aws_s3_bucket.frontend.bucket_domain_name
    origin_id   = aws_s3_bucket.frontend.id
  }

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.frontend.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2019"
  }

  default_cache_behavior {
    allowed_methods        = ["HEAD", "GET"]
    cached_methods         = ["HEAD", "GET"]
    viewer_protocol_policy = "redirect-to-https"
    target_origin_id       = aws_s3_bucket.frontend.id
    compress               = true

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  # This prevents index.html from being cached, we don't want index.html to be cached so that releases are picked up instantly.
  # Other resources can be cached because their paths will change across releases, e.g. main.07c350af.chunk.js.
  ordered_cache_behavior {
    path_pattern           = "/index.html"
    allowed_methods        = ["HEAD", "GET"]
    cached_methods         = ["HEAD", "GET"]
    viewer_protocol_policy = "redirect-to-https"
    target_origin_id       = aws_s3_bucket.frontend.id
    compress               = true
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  custom_error_response {
    error_code         = "404"
    response_code      = "200"
    response_page_path = "/index.html"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    project = "all-torso-no-trousers"
  }
}


resource "aws_route53_record" "frontend" {

  name    = "alltorsonotrousers.com"
  zone_id = aws_route53_zone.domain.zone_id
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.frontend.domain_name
    zone_id                = aws_cloudfront_distribution.frontend.hosted_zone_id
    evaluate_target_health = false
  }
}
