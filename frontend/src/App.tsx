import {RouteComponentProps, Router} from '@reach/router';
import React, {ReactNode} from 'react';
import CreateGroup from './admin-pages/create-group/CreateGroup';
import SecretLinks from './admin-pages/secret-links/SecretLinks';
import styles from './App.module.css';
import {DataProvider} from './data-context/DataContext';
import Footer from './furniture/footer/Footer';
import Header from './furniture/header/Header';
import HomePage from './home-page/HomePage';
import ArrangeByHeight from './member-pages/arrange-by-height/ArrangeByHeight';
import Results from './member-pages/results/Results';
import Summary from './member-pages/summary/Summary';
import YourOwnHeight from './member-pages/your-own-height/YourOwnHeight';
import UnrecoverableErrorPage from './unrecoverable-error-page/UnrecoverableErrorPage';

type RouteProps = RouteComponentProps & { children?: ReactNode }

function App() {

    const GroupSection = (props: RouteProps) => <>{props.children}</>;
    const MemberSection = (props: RouteProps) => <>{props.children}</>;
    const CreateGroupRoute = (props: RouteProps) => <CreateGroup/>;
    const SecretLinksRoute = (props: RouteProps) => <SecretLinks/>;
    const YourOwnHeightRoute = (props: RouteProps) => <YourOwnHeight/>;
    const ArrangeByHeightRoute = (props: RouteProps) => <ArrangeByHeight/>;
    const SummaryRoute = (props: RouteProps) => <Summary/>
    const ResultsRoute = (props: RouteProps) => <Results/>
    const UnrecoverableErrorRoute = (props: RouteProps) => <UnrecoverableErrorPage/>
    const HomePageRoute = (props: RouteProps) => <HomePage/>;

    return <div className={styles.page}>
        <Header/>
        <main className={styles.main + ' container'}>
            <DataProvider>
                <Router>
                    <CreateGroupRoute path="/groups/create"/>
                    <GroupSection path="/groups/:groupId">
                        <MemberSection path="/members/:memberId/secret/:memberSecret">
                            <SummaryRoute path="/"/>
                            <YourOwnHeightRoute path="/your-own-height"/>
                            <ArrangeByHeightRoute path="/arrange-by-height"/>
                            <ResultsRoute path="/results"/>
                        </MemberSection>
                        <SecretLinksRoute path="/secret/:groupSecret"/>
                    </GroupSection>
                    <UnrecoverableErrorRoute path="/error"/>
                    <HomePageRoute default/>
                </Router>
            </DataProvider>
        </main>
        <Footer/>
    </div>;
}

export default App;
