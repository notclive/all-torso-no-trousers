export class UnrecoverableError extends Error {
    public constructor(message: string) {
        super(message);
    }
}