import {useParams} from '@reach/router';
import {GroupResponse} from 'all-torso-no-trousers-backend/src/queries/get-group';
import {useData} from '../DataContext';
import {UnrecoverableError} from '../UnrecoverableError';

export function useGroup() {

    const params = useParams();

    function fetchGroup() {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/groups/${params.groupId}`, {
            headers: {
                'Authorization': params.memberSecret
            }
        }).then(response => {
            if (response.ok) {
                return response.json() as unknown as GroupResponse;
            }
            if (response.status > 399 && response.status < 500) {
                throw new UnrecoverableError(`Failed to fetch group due to client error, unexpected response status '${response.statusText}'`);
            }
            throw new Error(`Failed to fetch group due to server error, unexpected response status '${response.statusText}'`);
        });
    }

    return useData(`group-${params.groupId}`, fetchGroup);
}
