import {useParams} from '@reach/router';
import {MemberForSelfResponse} from 'all-torso-no-trousers-backend/src/queries/get-self';
import {useData} from '../DataContext';
import {UnrecoverableError} from '../UnrecoverableError';

export function useSelf() {

    const params = useParams();

    function fetchSelf() {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/groups/${params.groupId}/members/${params.memberId}`, {
            headers: {
                'Authorization': params.memberSecret
            }
        }).then(response => {
            if (response.ok) {
                return response.json() as unknown as MemberForSelfResponse;
            }
            if (response.status > 399 && response.status < 500) {
                throw new UnrecoverableError(`Failed to fetch member due to client error, unexpected response status '${response.statusText}'`);
            }
            throw new Error(`Failed to fetch member due to server error, unexpected response status '${response.statusText}'`);
        });
    }

    return useData(`self-${params.groupId}-${params.memberId}`, fetchSelf);
}
