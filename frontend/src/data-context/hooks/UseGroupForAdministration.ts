import {useParams} from '@reach/router';
import {GroupForAdministrationResponse} from 'all-torso-no-trousers-backend/src/queries/get-group-for-administration';
import {useData} from '../DataContext';
import {UnrecoverableError} from '../UnrecoverableError';

export function useGroupForAdministation() {

    const params = useParams();

    function fetchGroup() {
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/groups/${params.groupId}/administration`, {
            headers: {
                'Authorization': params.groupSecret
            }
        }).then(response => {
            if (response.ok) {
                return response.json() as unknown as GroupForAdministrationResponse;
            }
            if (response.status > 399 && response.status < 500) {
                throw new UnrecoverableError(`Failed to fetch group due to client error, unexpected response status '${response.statusText}'`);
            }
            throw new Error(`Failed to fetch group due to server error, unexpected response status '${response.statusText}'`);
        });
    }

    return useData(`group-for-administration-${params.groupId}`, fetchGroup);
}
