import {navigate} from '@reach/router';
import {createContext, ReactNode, useCallback, useContext, useEffect, useState} from 'react';
import {RequestState} from '../furniture/RequestState';
import LoadingSpinner from '../furniture/loading-spinner/LoadingSpinner';
import {UnrecoverableError} from './UnrecoverableError';
import styles from './DataContext.module.css';

interface Datum<T> {
    dataFetcher: () => Promise<T>;
    requestState: RequestState;
    value: T | undefined;
}

interface Data {
    [key: string]: Datum<any>;
}

const Context = createContext<[Data, (key: string, dataFetcher: () => Promise<any>) => void]>(
    [{}, () => {}]
);

export function DataProvider({children}: { children: ReactNode }) {

    const [data, setData] = useState<Data>({});

    const setDatum = useCallback((key: string, requestState: RequestState, value?: any) => {
        setData((previous) => {
            const datum = previous[key];
            return {
                ...previous,
                [key]: {
                    ...datum,
                    requestState,
                    value
                }
            };
        });
    }, [setData]);

    function initialiseKey(key: string, dataFetcher: () => Promise<any>) {
        setData((previous) => {
            return {
                ...previous,
                [key]: {
                    dataFetcher,
                    requestState: RequestState.NotStarted,
                    value: undefined
                }
            };
        });
    }

    const fetchData = useCallback((key: string) => {
        setDatum(key, RequestState.InProgress);
        data[key].dataFetcher()
            .then(value => {
                setDatum(key, RequestState.Successful, value);
            })
            .catch(error => {
                console.error(error);
                if (error instanceof UnrecoverableError) {
                    // Clear data to stop any new requests or errors.
                    setData({});
                    navigate('/error');
                } else {
                    setDatum(key, RequestState.Failed)
                }
            });
    }, [data, setDatum]);

    useEffect(() => {
        Object.entries(data)
            .filter(([, d]) => d.requestState === RequestState.NotStarted)
            .forEach(e => fetchData(e[0]));

    }, [data, fetchData]);

    const failedRequest = Object.entries(data).find(
        ([, d]) => d.requestState === RequestState.Failed
    );
    if (failedRequest) {
        return <div className={styles.whitespace}>
            <p>Failed to load data. This is probably a temporary problem.</p>
            <button onClick={() => fetchData(failedRequest[0])}>Try again</button>
        </div>;
    }

    if (Object.values(data).some(s => s.requestState === RequestState.InProgress)) {
        return <div className={styles.whitespace}>
            <LoadingSpinner/>
        </div>;
    }

    return <Context.Provider value={[data, initialiseKey]}>
        {children}
    </Context.Provider>;
}

export function useData<T>(key: string, dataFetcher: () => Promise<T>): [T | undefined, () => void] {

    const [data, initialiseKey] = useContext(Context);
    const datum = data[key];

    const purgeValue = useCallback(() => {
        initialiseKey(key, dataFetcher)
    }, [initialiseKey, key, dataFetcher]);

    useEffect(() => {
        if (!datum) {
            purgeValue();
        }
    }, [datum, purgeValue]);

    const value = datum?.requestState === RequestState.Successful
        ? datum.value as T
        : undefined;

    return [value, purgeValue];
}
