import {ReactNode} from 'react';
import styles from './ErrorMessage.module.css';

function ErrorMessage({children}: { children: ReactNode }) {

    if (!children) {
        return <></>;
    }

    return <div role="alert" className={styles.error}>
        ⚠&nbsp;&nbsp;{children}
    </div>;
}

export default ErrorMessage;