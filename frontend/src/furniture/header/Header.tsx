import {Link} from '@reach/router';
import {ReactComponent as Logo} from '../../images/underwear-logo.svg';
import './Header.css'

function Header() {
    return (
        <header>
            <div className="container">
                <Link to="/" aria-hidden={true}><Logo/></Link>
                <Link to="/"><h1>All torso and no trousers</h1></Link>
            </div>
        </header>
    );
}

export default Header;
