export enum RequestState {
    NotStarted,
    InProgress,
    Successful,
    Failed
}
