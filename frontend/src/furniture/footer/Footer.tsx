import {Link} from '@reach/router';
import {ReactComponent as Logo} from '../../images/underwear-logo.svg';
import styles from './Footer.module.css';

function Footer() {
    return (
        <footer className={styles.footer}>
            <div className="container">
                <Link to="/groups/create">Create your own group</Link>
                <Logo aria-hidden={true} />
                Made by&nbsp;<a href='mailto:notclive@protonmail.com'>Jonathan Price</a>
            </div>
        </footer>
    );
}

export default Footer;
