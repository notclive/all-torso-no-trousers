import styles from './LoadingSpinner.module.css';

function LoadingSpinner() {
    return <div className={styles.spinner}/>;
}

export default LoadingSpinner;
