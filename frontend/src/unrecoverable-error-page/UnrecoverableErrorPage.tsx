function UnrecoverableErrorPage() {
    return <div>
        <h2>Something went wrong</h2>
        Try starting again from your secret URL.
    </div>;
}

export default UnrecoverableErrorPage;
