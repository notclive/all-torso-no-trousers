import {Link} from '@reach/router';
import {useTitle} from 'react-use';
import {ReactComponent as Banner} from '../images/banner.svg';
import styles from './HomePage.module.css';

function HomePage() {

    useTitle('All torso and no trousers');

    return <div>
        <h2>How tall are your co-workers?</h2>
        Because of the pandemic you've probably been working from home.<br/>
        You may not have met all of your co-workers in person, at least not recently.<br/>
        You probably have an idea of how tall your co-workers are, let's see if you're correct.
        <p className={styles.banner}>
            <Banner/>
        </p>
        <h3>How it works</h3>
        <ol>
            <li>You create a group</li>
            <li>You share a private link with each member in your group</li>
            <li>Everyone in your group submits their height and arranges the group from
                tallest to shortest
            </li>
            <li>Once everyone has guessed, the website calculates who made the best guess, plus a few other
                stats
            </li>
        </ol>
        <Link className="button" to="/groups/create">Create a group</Link>
        <br/>
        <br/>
    </div>;
}

export default HomePage;
