import {Link, Redirect, useParams} from '@reach/router';
import {useTitle} from 'react-use';
import {useGroup} from '../../data-context/hooks/UseGroup';
import {useSelf} from '../../data-context/hooks/UseSelf';

function Summary() {

    useTitle('You\'re all done');

    const params = useParams();
    const [self] = useSelf();
    const [group, purgeGroup] = useGroup();

    if (!self || !group) {
        return <></>;
    }

    if (group.results) {
        return <Redirect to="./results" noThrow={true}/>;
    }

    if (!self.heightInCm) {
        return <Redirect to="./your-own-height" noThrow={true}/>;
    }

    if (!self.memberIdsFromTallestToShortest) {
        return <Redirect to="./arrange-by-height" noThrow={true}/>;
    }

    const stillToGuess = group.members
        .filter(m => !m.hasGuessed)
        .filter(m => m.id !== params.memberId)
        .map(m => m.name)
        .sort();

    // So that last guesser is shown results.
    if (stillToGuess.length === 0) {
        purgeGroup();
    }

    return <div>
        <h2>You're all done</h2>
        Return to this page once everyone has guessed to see the results.
        <h3>Change your mind</h3>
        <Link to="./your-own-height">Change my height</Link><br/>
        <Link to="./arrange-by-height">Change my guess</Link>
        <h3>Still to guess</h3>
        We're still waiting for the following to guess, give them a nudge.
        <ul>
            {stillToGuess.map(name => <li key={name}>{name}</li>)}
        </ul>
    </div>
}

export default Summary;