import {FormEvent, useState} from 'react';
import {navigate, useParams} from '@reach/router';
import {useTitle} from 'react-use';
import {RequestState} from '../../furniture/RequestState';
import {useSelf} from '../../data-context/hooks/UseSelf';
import {useGroup} from '../../data-context/hooks/UseGroup';
import ErrorMessage from '../../furniture/error-message/ErrorMessage';
import './YourOwnHeight.css';

function YourOwnHeight() {

    useTitle('Your own height');

    const params = useParams();
    const [self, purgeSelf] = useSelf();
    const [group] = useGroup();

    const [height, setHeight] = useState<string>(self?.heightInCm?.toString() || '');
    const [warning, setWarning] = useState<string>('');
    const [persistRequestState, setPersistRequestState] = useState<RequestState>(RequestState.NotStarted);

    function submit(event: FormEvent) {

        // Prevent page navigation.
        event.preventDefault();
        const heightInCm = getValidHeightInCm();
        setWarning(generateWarning());

        if (heightInCm && persistRequestState !== RequestState.InProgress) {
            return persistHeight(heightInCm);
        }
    }

    function persistHeight(heightInCm: number) {

        setPersistRequestState(RequestState.InProgress);

        return fetch(`${process.env.REACT_APP_BACKEND_URL}/groups/${params.groupId}/members/${params.memberId}/height`, {
            method: 'PUT',
            headers: {
                'Authorization': params.memberSecret,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                heightInCm
            })
        }).then(response => {
            if (response.ok) {
                // So that user isn't prompted for height again.
                purgeSelf();
                setPersistRequestState(RequestState.Successful);
                return navigate('./');
            }
            throw new Error(`Failed to persist height, unexpected response status '${response.statusText}'`);
        }).catch(error => {
            setPersistRequestState(RequestState.Failed);
            console.error(error);
        });
    }

    function getValidHeightInCm() {
        const heightInCm = Number.parseInt(height);
        if (!isNaN(heightInCm) && heightInCm > 0) {
            return heightInCm;
        }
    }

    function heightInFeetAndInches() {
        const heightInCm = getValidHeightInCm();
        if (!heightInCm) {
            return;
        }

        const heightInInches = heightInCm / 2.54;
        let wholeFeet = Math.floor(heightInInches / 12);
        let remainingInches = Math.round(heightInInches % 12);
        if (remainingInches === 12) {
            remainingInches = 0;
            wholeFeet++;
        }

        return `(${wholeFeet} foot${remainingInches > 0 ? ' ' + remainingInches : ''})`;
    }

    function generateWarning() {
        return !getValidHeightInCm() ? 'Height must be a positive number' : '';
    }

    function buttonLabel() {
        switch (persistRequestState) {
            case RequestState.Failed:
                return 'Try again';
            case RequestState.InProgress:
                return 'Submitting...';
            default:
                return 'Submit';
        }
    }

    if (!group) {
        return <></>;
    }

    return <form onSubmit={submit} noValidate={true}>
        <h2>Your own height</h2>
        <p>
            Because of the pandemic you probably haven't met every member of <i>{group.name}</i> in person.<br/>
            So let's see who can make the best guess at arranging the group from tallest to shortest.
        </p>
        <p>
            Before you make your guess we need to know how tall you are.<br/>
            Using this we can determine who made the best guess.
        </p>
        <p>
            <label>
                Your height{' '}
                <input inputMode="numeric" value={height}
                       size={3} maxLength={3} required={true}
                       aria-invalid={warning !== ''} aria-describedby="height-warning"
                       onChange={(e) => setHeight(e.target.value)}
                /> cm{' '}
            </label>
            <i>{heightInFeetAndInches()}</i>
            <span id="height-warning" role="alert">
                {warning}
            </span>
        </p>
        {
            persistRequestState === RequestState.Failed && <ErrorMessage>
                Failed to save your height. This is probably a temporary problem.
            </ErrorMessage>
        }
        <p>
            <button>{buttonLabel()}</button>
        </p>
    </form>;
}

export default YourOwnHeight;
