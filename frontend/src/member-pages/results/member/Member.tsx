import {useParams} from '@reach/router';
import {ActualHeight} from 'all-torso-no-trousers-backend/src/queries/get-group';
import styles from './Member.module.css';

export type HigherOrLower = 'did-not-guess' | 'higher' | 'lower' | 'same';

function Member(props: Props) {

    const params = useParams();

    return <div className={`member ${styles.member}`}>
        <span>
            <b>
                {props.member.memberId === params.memberId ? 'You' : props.member.name}
            </b>
            {' '}
            {props.member.heightInCm}cm
            {' '}
            {props.isTallest && '(tallest)'}
            {props.isShortest && '(shortest)'}
        </span>
        {props.youSaid !== 'did-not-guess' && <span className={`you-said ${styles.said}`}>
            You said
            <i className="material-icons" aria-hidden={true}>
                {props.youSaid === 'higher' && 'arrow_upward'}
                {props.youSaid === 'same' && 'check'}
                {props.youSaid === 'lower' && 'arrow_downward'}
            </i>
            <span className="screen-reader-only">
                {props.youSaid}
            </span>
        </span>}
    </div>;
}

interface Props {
    member: ActualHeight;
    isTallest?: boolean;
    isShortest?: boolean;
    youSaid: HigherOrLower;
}

export default Member;
