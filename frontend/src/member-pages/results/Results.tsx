import {Redirect, useParams} from '@reach/router';
import {useTitle} from 'react-use';
import {useGroup} from '../../data-context/hooks/UseGroup';
import {useSelf} from '../../data-context/hooks/UseSelf';
import Member from './member/Member';

function Results() {

    useTitle('The results');

    const params = useParams();
    const [self] = useSelf();
    const [group] = useGroup();

    if (!self || !group) {
        return <></>;
    }

    if (!group.results) {
        return <Redirect to="./summary" noThrow={true}/>;
    }

    function lookupName({memberId}: { memberId: string }) {
        if (memberId === params.memberId) {
            return 'You';
        }
        return group!.members.find(m => m.id === memberId)!.name;
    }

    function findActualPosition(memberId: string) {
        return group!.results!.actualHeightsFromTallestToShortest.findIndex(m => m.memberId === memberId) + 1;
    }

    function findGuessedPlacement(memberId: string) {
        return self!.memberIdsFromTallestToShortest!.indexOf(memberId) + 1;
    }

    function calculatePersonalPositionsOff(memberId: string) {
        return findActualPosition(memberId) - findGuessedPlacement(memberId);
    }

    function calculateSumOfPersonalPositionsOff() {
        return group?.members
            .map(m => Math.abs(calculatePersonalPositionsOff(m.id)))
            .reduce((a, b) => a + b);
    }

    function generateBestGuesserText() {
        const {memberId, positionsOff} = group!.results!.bestGuesser;
        return memberId === params.memberId
            ? `Your guess was only off by a total of ${positionsOff} positions.`
            : `${lookupName({memberId})}'s guess was only off by a total of ${positionsOff} positions.
               ${generatePersonalPositionsOffText()}`;
    }

    function generatePersonalPositionsOffText() {
        return self?.memberIdsFromTallestToShortest
            ? `Your guess was off by ${calculateSumOfPersonalPositionsOff()}.`
            : '';
    }

    function generateMostOverestimatedText() {
        const {memberId, averagePlacement} = group!.results!.mostOverestimated;
        return memberId === params.memberId
            ? `You were position ${findActualPosition(memberId)} when ordered from tallest to shortest,
            but on average the group placed you at position ${averagePlacement}. ${generatePersonalPlacementText(memberId)}`
            : `${lookupName({memberId})} was position ${findActualPosition(memberId)} when ordered from tallest to shortest,
            but on average the group placed them at position ${averagePlacement}. ${generatePersonalPlacementText(memberId)}`;
    }

    function generateMostUnderestimatedText() {
        const {memberId, averagePlacement} = group!.results!.mostUnderestimated;
        return memberId === params.memberId
            ? `You were position ${findActualPosition(memberId)} when ordered from tallest to shortest,
               but on average the group placed you at position ${averagePlacement}.  ${generatePersonalPlacementText(memberId)}`
            : `${lookupName({memberId})} was position ${findActualPosition(memberId)} when ordered from tallest to shortest,
               but on average the group placed them at position ${averagePlacement}. ${generatePersonalPlacementText(memberId)}`;
    }

    function generatePersonalPlacementText(memberId: string) {
        if (!self?.memberIdsFromTallestToShortest) {
            return '';
        }
        return memberId === params.memberId
            ? `You placed yourself at position ${findGuessedPlacement(memberId)}.`
            : `You placed them at position ${findGuessedPlacement(memberId)}.`;
    }

    function generateBiggestOverestimateOfOwnHeightText() {
        const {memberId, positionsOff} = group!.results!.biggestOverestimateOfOwnHeight;
        if (memberId === params.memberId) {
            return `You placed yourself ${positionsOff} positions above your actual position.`;
        }

        return `${lookupName({memberId})} placed themselves ${positionsOff} positions above their actual position.
                ${generatePersonalOverestimateOfOwnHeightText()}`;
    }

    function generatePersonalOverestimateOfOwnHeightText() {
        if (!self?.memberIdsFromTallestToShortest) {
            return '';
        }

        const personalPositionsOff = calculatePersonalPositionsOff(params.memberId);
        const overOrUnder = personalPositionsOff >= 0 ? 'above' : 'below';
        return `You placed yourself ${Math.abs(personalPositionsOff)} positions ${overOrUnder} your actual position.`;
    }

    function judgeGuessOfMember(memberId: string) {
        if (!self?.memberIdsFromTallestToShortest) {
            return 'did-not-guess';
        }

        const guessedPlacement = findGuessedPlacement(memberId);
        const actualPosition = findActualPosition(memberId);
        // Un-intuitively a lower placement means the guess was higher.
        if (guessedPlacement < actualPosition) {
            return 'higher';
        }
        if (guessedPlacement > actualPosition) {
            return 'lower';
        }
        return 'same';
    }

    return <div>
        <h2>The results</h2>
        <p>
            Everyone has guessed, let the other members know that the results are ready.<br/>
            They can view the results using the same URL they used to make their guess.
        </p>
        <h3>🏆 Top guesser: {lookupName(group.results.bestGuesser)}</h3>
        {generateBestGuesserText()}
        <h3>📉 Most overestimated: {lookupName(group.results.mostOverestimated)}</h3>
        {generateMostOverestimatedText()}
        <h3>🦒 Most underestimated: {lookupName(group.results.mostUnderestimated)}</h3>
        {generateMostUnderestimatedText()}
        <h3>😳 Biggest overestimate of their own height: {lookupName(group.results.biggestOverestimateOfOwnHeight)}</h3>
        {generateBiggestOverestimateOfOwnHeightText()}
        <h3>The actual height order</h3>
        {group.results.actualHeightsFromTallestToShortest.map((member, index) =>
            <Member key={member.memberId}
                    member={member}
                    isTallest={index === 0}
                    isShortest={index === group?.members.length - 1}
                    youSaid={judgeGuessOfMember(member.memberId)}/>
        )}
        <br/>
    </div>
}

export default Results;