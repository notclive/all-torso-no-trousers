import {
    closestCenter,
    DndContext,
    DragEndEvent,
    DragOverlay,
    DragStartEvent,
    KeyboardSensor,
    PointerSensor,
    useSensor,
    useSensors,
} from '@dnd-kit/core';
import {arrayMove, SortableContext, sortableKeyboardCoordinates, verticalListSortingStrategy,} from '@dnd-kit/sortable';
import {navigate, useParams} from '@reach/router';
import {FormEvent, useCallback, useEffect, useState} from 'react';
import {useTitle} from 'react-use';
import {useGroup} from '../../data-context/hooks/UseGroup';
import {useSelf} from '../../data-context/hooks/UseSelf';
import ErrorMessage from '../../furniture/error-message/ErrorMessage';
import {RequestState} from '../../furniture/RequestState';
import ArrangeableMember from './arrangeable-member/ArrangeableMember';
import DraggableMember from './draggable-member/DraggableMember';

function ArrangeByHeight() {

    useTitle('Arrange by height');

    const params = useParams();
    const [group] = useGroup();
    const [self, purgeSelf] = useSelf();
    const [activeId, setActiveId] = useState<string | null>(null);
    const [sortedMembers, setSortedMembers] = useState<string[]>([]);
    const [persistRequestState, setPersistRequestState] = useState<RequestState>(RequestState.NotStarted);

    const compareUsingPreviousGuess = useCallback((a: string, b: string) => {
        const previousGuess = self?.memberIdsFromTallestToShortest || [];
        return previousGuess.indexOf(a) - previousGuess.indexOf(b);
    }, [self?.memberIdsFromTallestToShortest]);

    useEffect(() => {
        const memberIds = group?.members.map(m => m.id) || [];
        setSortedMembers(memberIds.sort(compareUsingPreviousGuess));
    }, [setSortedMembers, group?.members, compareUsingPreviousGuess]);

    const keyboardAndMouse = useSensors(
        useSensor(PointerSensor),
        useSensor(KeyboardSensor, {
            coordinateGetter: sortableKeyboardCoordinates,
        })
    );

    function handleDragStart({active}: DragStartEvent) {
        setActiveId(active.id);
    }

    function handleDragEnd({active, over}: DragEndEvent) {

        if (over === null || active.id === over.id) {
            return;
        }

        setSortedMembers(sortedMembers => {
            const oldIndex = sortedMembers.indexOf(active.id);
            const newIndex = sortedMembers.indexOf(over.id);
            return arrayMove(sortedMembers, oldIndex, newIndex);
        });

        setActiveId(null);
    }

    function submitGuess(event: FormEvent) {

        // Prevent page navigation.
        event.preventDefault();
        if (persistRequestState === RequestState.InProgress) {
            return;
        }
        setPersistRequestState(RequestState.InProgress);

        fetch(`${process.env.REACT_APP_BACKEND_URL}/groups/${params.groupId}/members/${params.memberId}/guess`, {
            method: 'PUT',
            headers: {
                'Authorization': params.memberSecret,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                memberIdsFromTallestToShortest: sortedMembers
            })
        }).then(response => {
            if (response.ok) {
                // So that user isn't prompted to guess again.
                purgeSelf();
                setPersistRequestState(RequestState.Successful);
                return navigate('./');
            }
            throw new Error(`Failed to persist guess, unexpected response status '${response.statusText}'`);
        }).catch(error => {
            setPersistRequestState(RequestState.Failed);
            console.error(error);
        });
    }

    function buttonLabel() {
        switch (persistRequestState) {
            case RequestState.Failed:
                return 'Try again';
            case RequestState.InProgress:
                return 'Submitting...';
            default:
                return 'I\'m done';
        }
    }

    function findMember(memberId: string) {
        return group?.members.find(m => m.id === memberId)!;
    }

    if (!group) {
        return <></>
    }

    return <div>
        <h2>Arrange by height</h2>
        <p>
            Arrange the members of <i>{group.name}</i> from tallest to shortest.<br/>
            Once everyone in the group has made their guess we will reveal who made the best guess.
        </p>
        <DndContext sensors={keyboardAndMouse}
                    collisionDetection={closestCenter}
                    onDragStart={handleDragStart}
                    onDragEnd={handleDragEnd}>
            <SortableContext items={sortedMembers}
                             strategy={verticalListSortingStrategy}>
                {sortedMembers.map((memberId, index) =>
                    <ArrangeableMember key={memberId}
                                       member={findMember(memberId)}
                                       numberOfMembers={sortedMembers.length}/>
                )}
            </SortableContext>
            <DragOverlay>
                {
                    activeId ? <DraggableMember member={findMember(activeId)}/> : null
                }
            </DragOverlay>
        </DndContext>
        {
            persistRequestState === RequestState.Failed && <ErrorMessage>
                Failed to save your guess. This is probably a temporary problem.
            </ErrorMessage>
        }
        <p>
            <button onClick={submitGuess}>
                {buttonLabel()}
            </button>
        </p>
    </div>;
}

export default ArrangeByHeight;
