import {useSortable} from '@dnd-kit/sortable';
import {CSS} from '@dnd-kit/utilities';
import {ArrangeableMemberResponse} from 'all-torso-no-trousers-backend/src/queries/get-group';
import Member from '../member/Member';
import styles from './ArrangeableMember.module.css';

function ArrangeableMember(props: Props) {

    const {
        attributes,
        listeners,
        setNodeRef,
        transform,
        transition,
        index,
        isDragging,
    } = useSortable({id: props.member.id});

    const style = {
        transform: CSS.Transform.toString(transform),
        transition: transition ? transition : undefined,
    };

    return <div className={`${styles.arrangeable} ${isDragging && styles.faded}`}
                ref={setNodeRef}
                style={style}>
        <Member member={props.member}
                listeners={listeners}
                attributes={attributes}
                isTallest={index === 0}
                isShortest={index === props.numberOfMembers - 1}/>
    </div>;
}

interface Props {
    member: ArrangeableMemberResponse;
    numberOfMembers: number;
}

export default ArrangeableMember;
