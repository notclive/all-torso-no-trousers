import {DraggableSyntheticListeners} from '@dnd-kit/core';
import {useParams} from '@reach/router';
import {ArrangeableMemberResponse} from 'all-torso-no-trousers-backend/src/queries/get-group';
import styles from './Member.module.css';

function Member(props: Props) {

    const params = useParams();

    return <div className={`member ${styles.member}`}>
        {/* Only the handle is draggable, to allow the screen to be scrolled on mobile. */}
        <span className={`handle ${styles.handle}`} {...props.attributes} {...props.listeners}>
            <span className="material-icons" aria-hidden="true">drag_handle</span>
        </span>
        <b className={styles.name}>
            {props.member.id === params.memberId ? 'You' : props.member.name}
        </b>
        {props.isTallest && '(tallest)'}
        {props.isShortest && '(shortest)'}
    </div>;
}

interface Props {
    member: ArrangeableMemberResponse;
    attributes?: {
        role: string;
        tabIndex: number;
        'aria-pressed': boolean | undefined;
        'aria-roledescription': string;
        'aria-describedby': string;
    };
    listeners?: DraggableSyntheticListeners;
    isTallest?: boolean;
    isShortest?: boolean;
}

export default Member;
