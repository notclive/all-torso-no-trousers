import {ArrangeableMemberResponse} from 'all-torso-no-trousers-backend/src/queries/get-group';
import Member from '../member/Member';
import styles from './DraggableMember.module.css';

function DraggableMember(props: Props) {
    return <div className={styles.draggable}>
        <Member {...props}/>
    </div>;
}

interface Props {
    member: ArrangeableMemberResponse;
}

export default DraggableMember;
