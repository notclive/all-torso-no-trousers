import {navigate} from '@reach/router';
import {FormEvent, useState} from 'react';
import {useTitle} from 'react-use';
import ErrorMessage from '../../furniture/error-message/ErrorMessage';
import {RequestState} from '../../furniture/RequestState';

function CreateGroup() {

    useTitle('Create your own group');

    const [groupName, setGroupName] = useState('');
    const [memberNames, setMemberNames] = useState<string[]>(Array(5).fill(''));
    const [warning, setWarning] = useState<string | undefined>('');
    const [createRequestState, setCreateRequestState] = useState<RequestState>(RequestState.NotStarted);

    function create(event: FormEvent) {

        // Prevent page navigation.
        event.preventDefault();

        const warning = generateWarning();
        setWarning(warning);
        if (warning || createRequestState === RequestState.InProgress) {
            return;
        }

        setCreateRequestState(RequestState.InProgress);
        return fetch(`${process.env.REACT_APP_BACKEND_URL}/groups`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                groupName: groupName.trim(),
                memberNames: notBlankMemberNames()
            })
        }).then(response => {
            if (response.ok) {
                setCreateRequestState(RequestState.Successful);
                return response.json();
            }
            throw new Error(`Failed to create group, unexpected response status '${response.statusText}'`);
        })
        .then(response => {
            return navigate(`./${response.groupId}/secret/${response.secret}`)
        }).catch(error => {
            setCreateRequestState(RequestState.Failed);
            console.error(error);
        });
    }

    function generateWarning() {
        if (!groupName.trim()) {
            return 'A group name is required';
        }
        if (memberNames[0].trim() === '') {
            return 'Your own name is required';
        }
        if (notBlankMemberNames().length < 3) {
            return 'At least 3 members are required';
        }
        const duplicate = findDuplicateMemberName();
        if (duplicate) {
            return `Member names should be unique, "${duplicate}" was entered twice`;
        }
    }

    function notBlankMemberNames() {
        return memberNames.map(n => n.trim()).filter(n => n.length > 0);
    }

    function findDuplicateMemberName() {
        const names = notBlankMemberNames();
        for (let indexA = 0; indexA < names.length; indexA++) {
            for (let indexB = indexA + 1; indexB < names.length; indexB++) {
                if (names[indexA] === names[indexB]) {
                    return names[indexA];
                }
            }
        }
    }

    function setMemberName(index: number, value: string) {
        setMemberNames(previous => {
            const updated = [...previous];
            updated[index] = value;
            return updated;
        });
    }

    function addMoreMembers() {
        setMemberNames(previous => [...previous, '', '', '', '', '']);
    }

    function buttonLabel() {
        switch (createRequestState) {
            case RequestState.Failed:
                return 'Try again';
            case RequestState.InProgress:
                return 'Creating...';
            default:
                return 'Create';
        }
    }

    return <form onSubmit={create} noValidate={true}>
        <h2>Create your own group</h2>
        <p>
            <label>
                Group name{' '}
                <input onChange={(event) => setGroupName(event.target.value)}
                       required={true}
                       placeholder="e.g. Acme sales team"/>
            </label>
        </p>
        <p>
            Every member in the group will need to provide their own height and guess at the height order of the group members.
            You will need to share a secret URL with each member allowing them to enter their guess.
        </p>
        <p>
            <label>
                Your name{' '}
                <input onChange={(event) => setMemberName(0, event.target.value)}
                       placeholder="e.g. Chloe"
                       required={true}/>
            </label>
        </p>
        <div>
            <label>
                Member names
                {
                    memberNames.slice(1).map((_, i) => i + 1).map(index => (<p key={index}>
                        <input id={`member-name-${index}`}
                               placeholder={index === 0 ? 'e.g. Dave' : ''}
                               onChange={(event) => setMemberName(index, event.target.value)}/>
                    </p>))
                }
            </label>
        </div>
        <p>
            {memberNames.length < 20 && <button onClick={addMoreMembers} type="button">
                Add more members
            </button>}
        </p>
        <ErrorMessage>
            {warning}
        </ErrorMessage>
        {
            createRequestState === RequestState.Failed && <ErrorMessage>
                Failed to create your group. This is probably a temporary problem.
            </ErrorMessage>
        }
        <p>
            <button>{buttonLabel()}</button>
        </p>
    </form>;
}

export default CreateGroup;