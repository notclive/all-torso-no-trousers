import {useState} from 'react';
import styles from './CopyableLink.module.css';

interface Props {
    path: string;
}

function CopyableLink(props: Props) {

    const url = `${window.location.protocol}//${window.location.hostname}${props.path}`;
    const [copied, setCopied] = useState(false);

    function copy() {
        setCopied(true);
        setTimeout(() => setCopied(false), 500);
        return navigator.clipboard.writeText(url);
    }

    return <span className={styles.container}>
        <span className={styles.url}>
            {url}
        </span>
        <button className={styles.copy} onClick={copy}>
            {copied ? 'Copied' : 'Copy'}
        </button>
    </span>;
}

export default CopyableLink;