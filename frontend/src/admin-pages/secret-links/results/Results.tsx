import {useParams} from '@reach/router';
import {useState} from 'react';
import {useGroupForAdministation} from '../../../data-context/hooks/UseGroupForAdministration';
import ErrorMessage from '../../../furniture/error-message/ErrorMessage';
import {RequestState} from '../../../furniture/RequestState';

function Results() {

    const params = useParams();
    const [group, purgeGroupForAdministration] = useGroupForAdministation();
    const [requestState, setRequestState] = useState<RequestState>(RequestState.NotStarted);
    const resultsState = determineResultsState();

    function determineResultsState() {
        if (group && group.resultsAreReleased) {
            return 'AVAILABLE';
        }
        if (!group || group.members.filter(m => m.hasGuessed).length < 3) {
            return 'NOT_ENOUGH_GUESSES';
        }
        return 'RELEASE_NOW';
    }

    function releaseResultsNowLabel() {
        switch (requestState) {
            case RequestState.Failed:
                return 'Try again';
            case RequestState.InProgress:
                return 'Releasing...';
            default:
                return 'Release results now';
        }
    }

    function releaseResultsNow() {

        setRequestState(RequestState.InProgress);

        return fetch(`${process.env.REACT_APP_BACKEND_URL}/groups/${params.groupId}/release-results-now`, {
            method: 'POST',
            headers: {
                'Authorization': params.groupSecret,
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (response.ok) {
                // So that results state is re-evaluated.
                purgeGroupForAdministration();
                setRequestState(RequestState.Successful);
            }
            throw new Error(`Failed to remove members, unexpected response status '${response.statusText}'`);
        }).catch(error => {
            setRequestState(RequestState.Failed);
            console.error(error);
        });
    }

    if (!group) {
        return <></>;
    }

    return <div>
        <h3>Results</h3>
        {resultsState === 'AVAILABLE' && <div>
            The results are available, each member can view their personalised results using their secret URL.
        </div>}
        {resultsState === 'NOT_ENOUGH_GUESSES' && <div>
            The results will be available once all members have made their guess.
        </div>}
        {resultsState === 'RELEASE_NOW' && <div>
            You can release the results now, but the following members will not a get a chance to guess.
            <ul>
                {group.members.filter(m => !m.hasGuessed).map(m => <li key={m.id}>{m.name}</li>)}
            </ul>
            {
                requestState === RequestState.Failed && <ErrorMessage>
                    Failed to release results. This is probably a temporary problem.
                </ErrorMessage>
            }
            <button onClick={releaseResultsNow}>{releaseResultsNowLabel()}</button>
        </div>}
        <br/>
    </div>;
}

export default Results;