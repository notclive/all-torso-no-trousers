import {useParams} from '@reach/router';
import {MemberForAdministrationResponse} from 'all-torso-no-trousers-backend/src/queries/get-group-for-administration';
import {useTitle} from 'react-use';
import {useGroupForAdministation} from '../../data-context/hooks/UseGroupForAdministration';
import CopyableLink from './copyable-link/CopyableLink';
import Results from './results/Results';
import styles from './SecretLinks.module.css';

function SecretLinks() {

    useTitle('Group administration');

    const params = useParams();
    const [group] = useGroupForAdministation();

    function compareByName(a: MemberForAdministrationResponse, b: MemberForAdministrationResponse) {
        return a.name < b.name ? -1 : 1;
    }

    if (!group) {
        return <></>
    }

    return <div>
        <h2>Group administration</h2>

        <h3>Secret administration URL</h3>
        This is your secret URL for group administration, do not share it with anyone.<br/>
        Save this URL so that you can return here later.
        <br/>
        <br/>
        <CopyableLink path={`/groups/${params.groupId}/secret/${params.groupSecret}`}/>
        <br/>

        <Results/>

        <h3>Secret member URLs</h3>
        These URLs permit the members of your group to enter their height and make their guess.<br/>
        Each URL is personal and should be shared only with that member of your group.
        {
            group.members.sort(compareByName).map(m =>
                <div key={m.id}>
                    <h4>{m.name}</h4>
                    <CopyableLink path={`/groups/${params.groupId}/members/${m.id}/secret/${m.secret}`}/>
                    <div className={styles.indicators}>
                        Has submitted their height:
                        <i className="material-icons" aria-hidden={true}>
                            {m.hasSubmittedHeight ? 'check' : 'clear'}
                        </i>
                        <span className="screen-reader-only">
                            {m.hasSubmittedHeight ? 'yes' : 'no'}
                        </span>
                    </div>
                    <div className={styles.indicators}>
                        Has guessed:
                        <i className="material-icons" aria-hidden={true}>
                            {m.hasGuessed ? 'check' : 'clear'}
                        </i>
                        <span className="screen-reader-only">
                            {m.hasGuessed ? 'yes' : 'no'}
                        </span>
                    </div>
                </div>
            )
        }
        <br/>

    </div>;
}

export default SecretLinks;