aws s3 sync ./build/ s3://all-torso-no-trousers-frontend --delete --no-progress --exclude 'index.html' --cache-control 'max-age=31536000, public'
aws s3 cp ./build/index.html s3://all-torso-no-trousers-frontend --no-progress --cache-control 'no-cache'
