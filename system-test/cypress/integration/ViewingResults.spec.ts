import {createGroup} from '../support/entity-builders/GroupBuilder';
import {createMember} from '../support/entity-builders/MemberBuilder';

describe('viewing results', () => {

    specify('when a member submits the final guess, then they are shown the results', () => {
        // given
        cy.then(async () => {
            const group = await createGroup().withId('cxD2Zpd09').save();
            await createMember().withId('op2dSa2Sl').withGroup(group)
                .withHeight(179)
                .withSecret('xIX1S91cCkKs')
                .withName('carl')
                .save();
            await createMember().withId('vkD2-aL92').withGroup(group)
                .withHeight(176)
                .withGuess(['op2dSa2Sl', 'vkD2-aL92', '11A0xAo22'])
                .withName('ada')
                .save();
            await createMember().withId('11A0xAo22').withGroup(group)
                .withHeight(172)
                .withName('Bob')
                .withGuess(['op2dSa2Sl', 'vkD2-aL92', '11A0xAo22'])
                .save();
        });

        // when
        cy.visit('http://localhost:3000/groups/cxD2Zpd09/members/op2dSa2Sl/secret/xIX1S91cCkKs');
        cy.contains('button', `I'm done`).click();

        // then
        cy.contains('The results').should('exist');
        cy.contains('Everyone has guessed').should('exist');
    });

    specify('given all members have guessed, then I can see the results', () => {
        // given
        cy.then(async () => {
            const group = await createGroup().withId('ld3Sj9cSw').save();
            await createMember().withId('tallest').withName('tallest')
                .withSecret('xIX1S91cCkKs').withGroup(group)
                .withHeight(187)
                .withGuess(['second', 'tallest', 'fourth', 'shortest', 'middle'])
                .save();
            await createMember().withId('second').withName('second').withGroup(group)
                .withHeight(176)
                .withGuess(['tallest', 'second', 'fourth', 'middle', 'shortest'])
                .save();
            await createMember().withId('middle').withName('middle').withGroup(group)
                .withHeight(169)
                .withGuess(['tallest', 'second', 'fourth', 'shortest', 'middle'])
                .save();
            await createMember().withId('fourth').withName('fourth').withGroup(group)
                .withHeight(156)
                .withGuess(['fourth', 'second', 'tallest', 'shortest', 'middle'])
                .save();
            await createMember().withId('shortest').withName('shortest').withGroup(group)
                .withHeight(156)
                .withGuess(['second', 'tallest', 'fourth', 'middle', 'shortest'])
                .save();
        });

        // when
        cy.visit('http://localhost:3000/groups/ld3Sj9cSw/members/tallest/secret/xIX1S91cCkKs');

        // then
        cy.contains('The results').should('exist');

        cy.contains('Top guesser: second');
        cy.contains(`second's guess was only off by a total of 2 positions`);

        cy.contains('Most overestimated: fourth');
        cy.contains('fourth was position 4 when ordered from tallest to shortest, but on average the group placed them at position 2.6');

        cy.contains('Most underestimated: middle');
        cy.contains('middle was position 3 when ordered from tallest to shortest, but on average the group placed them at position 4.6');

        cy.contains('Biggest overestimate of their own height: fourth');
        cy.contains('fourth placed themselves 3 positions above their actual position');
    });

    specify('when I appear in the results, then I see "you" instead of my name', () => {
        // given
        cy.then(async () => {
            const group = await createGroup().withId('vm2xX0oOw').save();
            await createMember().withId('cl2x9sOw1').withSecret('xIX1S91cCkKs')
                .withGroup(group)
                .withHeight(187)
                .withGuess(['cl2x9sOw1', 'c31OdcDr9'])
                .save();
            await createMember().withId('c31OdcDr9').withName('Roger')
                .withGroup(group)
                .withHeight(176)
                .withGuess(['c31OdcDr9', 'cl2x9sOw1'])
                .save();
        });

        // when
        cy.visit('http://localhost:3000/groups/vm2xX0oOw/members/cl2x9sOw1/secret/xIX1S91cCkKs');

        // then
        cy.contains('Top guesser: You').should('exist');
        cy.contains('Your guess was').should('exist');
        cy.contains('Most overestimated: Roger').should('exist');
        cy.contains('Roger was').should('exist');
    });

    specify('when I view the results, then the results are personalised to me', () => {
        // given
        cy.then(async () => {
            const group = await createGroup().withId('ld3Sj9cSw').save();
            await createMember().withId('tallest').withName('tallest')
                .withSecret('xIX1S91cCkKs').withGroup(group)
                .withHeight(187)
                .withGuess(['second', 'tallest', 'fourth', 'shortest', 'middle'])
                .save();
            await createMember().withId('second').withName('second').withGroup(group)
                .withHeight(176)
                .withGuess(['tallest', 'second', 'fourth', 'middle', 'shortest'])
                .save();
            await createMember().withId('middle').withName('middle').withGroup(group)
                .withHeight(169)
                .withGuess(['tallest', 'second', 'fourth', 'shortest', 'middle'])
                .save();
            await createMember().withId('fourth').withName('fourth').withGroup(group)
                .withHeight(156)
                .withGuess(['fourth', 'second', 'tallest', 'shortest', 'middle'])
                .save();
            await createMember().withId('shortest').withName('shortest').withGroup(group)
                .withHeight(156)
                .withGuess(['second', 'tallest', 'fourth', 'middle', 'shortest'])
                .save();
        });

        // when
        cy.visit('http://localhost:3000/groups/ld3Sj9cSw/members/tallest/secret/xIX1S91cCkKs');

        // then
        cy.contains('Your guess was off by 6').should('exist');
        cy.contains('You placed them at position 3').should('exist');
        cy.contains('You placed them at position 5').should('exist');
        cy.contains('You placed yourself 1 positions below your actual position.').should('exist');
    });

    specify('when I view the results, then I can see how my guess compared to the actual order', () => {
        // given
        cy.then(async () => {
            const group = await createGroup().withId('v9sW2u2NS').save();
            await createMember().withId('tallest').withName('tallest')
                .withSecret('xIX1S91cCkKs').withGroup(group)
                .withHeight(187)
                .withGuess(['shortest', 'middle', 'tallest'])
                .save();
            await createMember().withId('middle').withName('middle').withGroup(group)
                .withHeight(169)
                .withGuess(['tallest', 'middle', 'shortest'])
                .save();
            await createMember().withId('shortest').withName('shortest').withGroup(group)
                .withHeight(150)
                .withGuess(['tallest', 'middle', 'shortest'])
                .save();
        });

        // when
        cy.visit('http://localhost:3000/groups/v9sW2u2NS/members/tallest/secret/xIX1S91cCkKs');

        // then
        cy.contains('.member', 'You 187cm (tallest)')
            .children('.you-said').should('contain', 'arrow_downward');
        cy.contains('.member', 'middle 169cm')
            .children('.you-said').should('contain', 'check');
        cy.contains('.member', 'shortest 150cm (shortest)')
            .children('.you-said').should('contain', 'arrow_upward');
    });

    specify('given I did not submit my height, when the results are released early, then I can still view the results', () => {
        // given
        cy.then(async () => {
            const group = await createGroup().withId('cnd221j0x').withResultsReleasedEarly().save();
            await createMember().withId('43fdDD9sW').withGroup(group)
                .withGuess(['43fdDD9sW', 'LPO929Swq', 'kw2Cn9SD2', 'com9m=2dw'])
                .withHeight(160).save();
            await createMember().withId('LPO929Swq').withGroup(group)
                .withGuess(['43fdDD9sW', 'LPO929Swq', 'kw2Cn9SD2', 'com9m=2dw'])
                .withHeight(155).save();
            await createMember().withId('com9m=2dw').withGroup(group)
                .withGuess(['43fdDD9sW', 'LPO929Swq', 'kw2Cn9SD2', 'com9m=2dw'])
                .withHeight(154).save();
            await createMember().withId('kw2Cn9SD2').withGroup(group)
                .withHeight(150).save();
            await createMember().withId('lcpOWsdOw').withGroup(group)
                .withSecret('dxcC2D0v3fKF').save();
        });

        // when
        cy.visit('http://localhost:3000/groups/cnd221j0x/members/lcpOWsdOw/secret/dxcC2D0v3fKF');

        // then
        cy.contains('The results');
        cy.contains('Top guesser: Charlie');
        cy.contains('.member', 'You').should('not.exist');
    });
});