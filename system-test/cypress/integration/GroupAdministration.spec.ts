import 'cypress-plugin-tab';
import {createGroup} from '../support/entity-builders/GroupBuilder';
import {createMember} from '../support/entity-builders/MemberBuilder';

describe('group administration', () => {

    specify('when I create a group with 6 members, then I see all members on the group administration screen', () => {
        // given
        cy.visit('http://localhost:3000');
        cy.contains('a', 'Create your own group').click();

        // when
        cy.contains('label', 'Group name').click();
        cy.focused().type('Acme IT team');

        cy.contains('label', 'Your name').click();
        cy.focused().type('Joey');
        cy.contains('label', 'Member names').click();
        cy.focused().type('Sandra');
        cy.focused().tab().type('Penelope');
        cy.focused().tab().type('Barry');
        cy.focused().tab().type('Axel');

        cy.contains('button', 'Add more members').click();
        cy.get('#member-name-5').type('Trev');

        cy.contains('button', 'Create').click();

        // then
        cy.contains('Group administration').should('exist');
        cy.contains('Joey').should('exist');
        cy.contains('Sandra').should('exist');
        cy.contains('Penelope').should('exist');
        cy.contains('Barry').should('exist');
        cy.contains('Axel').should('exist');
        cy.contains('Trev').should('exist');
    });

    specify('given a group has been created, then an admin can copy a member\'s secret URL', () => {
        // given
        cy.then(async () => {
            const group = await createGroup().withId('c9J92Bw8d').withSecret('lO0d7s22BXdoi').save();
            await createMember().withId('op2dSa2Sl').withSecret('xIX1S91cCkKs').withGroup(group).save();
        });

        // when
        cy.visit('http://localhost:3000/groups/c9J92Bw8d/secret/lO0d7s22BXdoi');
        cy.contains('http://localhost/groups/c9J92Bw8d/members/op2dSa2Sl/secret/xIX1S91cCkKs')
            .next('button')
            .click()

        // then
        cy.task('getClipboard')
            .should('equal', 'http://localhost/groups/c9J92Bw8d/members/op2dSa2Sl/secret/xIX1S91cCkKs');
    });

    specify('when I provide the wrong secret, then I can not see secret URLs', () => {
        // given
        cy.then(async () => {
            await createGroup().withId('c9J92Bw8d').save();
        });

        // when
        cy.visit('http://localhost:3000/groups/c9J92Bw8d/secret/cQ033122Bxr4i');

        // then
        cy.contains('Try starting again from your secret URL').should('exist');
    });

    specify('given some members have not submitted their height, then I can release the results now', () => {
        // given
        cy.then(async () => {
            const group = await createGroup().withId('vUwe2S0xc').withSecret('c09d3qdjc32S').save();
            await createMember().withId('43fdDD9sW').withGroup(group)
                .withGuess(['43fdDD9sW', 'lcpOWsdOw', 'LPO929Swq', 'kw2Cn9SD2', 'com9m=2dw'])
                .withHeight(160).save();
            await createMember().withId('LPO929Swq').withGroup(group)
                .withGuess(['43fdDD9sW', 'lcpOWsdOw', 'LPO929Swq', 'kw2Cn9SD2', 'com9m=2dw'])
                .withHeight(155).save();
            await createMember().withId('com9m=2dw').withGroup(group)
                .withGuess(['43fdDD9sW', 'lcpOWsdOw', 'LPO929Swq', 'kw2Cn9SD2', 'com9m=2dw'])
                .withHeight(154).save();
            await createMember().withId('lcpOWsdOw').withGroup(group)
                .withHeight(150).save();
            await createMember().withId('kw2Cn9SD2').withGroup(group)
                .withName('Boris').save();
        });

        // when
        cy.visit('http://localhost:3000/groups/vUwe2S0xc/secret/c09d3qdjc32S');
        cy.contains('button', 'Release results now').click();

        // then
        cy.contains('The results are available').should('exist');
    });
})
