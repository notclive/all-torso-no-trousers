import 'cypress-plugin-tab';
import {createGroup} from '../support/entity-builders/GroupBuilder';
import {createMember} from '../support/entity-builders/MemberBuilder';

describe('submitting a guess', () => {

    specify('when I visit a member link, then I can submit my height and guess', () => {
        // given
        cy.then(async () => {
            const group = await createGroup().withId('c9J92Bw8d').save();
            await createMember().withId('op2dSa2Sl').withSecret('xIX1S91cCkKs').withGroup(group).save();
            await createMember().withGroup(group).save();
        });

        // when
        cy.visit('http://localhost:3000/groups/c9J92Bw8d/members/op2dSa2Sl/secret/xIX1S91cCkKs/your-own-height');
        whenISubmitMyHeight('165');

        // Can't rely on automatic navigation, guess may have already been submitted by previous test.
        cy.visit('http://localhost:3000/groups/c9J92Bw8d/members/op2dSa2Sl/secret/xIX1S91cCkKs/arrange-by-height');
        // Using keyboard interaction because I can't get pointer events to work.
        // See https://github.com/cypress-io/cypress/issues/845.
        cy.contains('.member', 'You').find('.handle').focus()
            .type(' ').type('{downArrow}').type(' ');
        cy.contains('button', `I'm done`).click();

        // then
        cy.contains(`You're all done`).should('exist');
    });

    specify('when I change my height, then I see my previously submitted value', () => {
        // given
        cy.then(async () => {
            const group = await createGroup().withId('cp92wd9xa').save();
            await createMember().withId('op2dSa2Sl').withSecret('xIX1S91cCkKs').withGroup(group).save();
        });

        // when
        cy.visit('http://localhost:3000/groups/cp92wd9xa/members/op2dSa2Sl/secret/xIX1S91cCkKs/your-own-height');
        whenISubmitMyHeight('172');

        // then
        cy.visit('http://localhost:3000/groups/cp92wd9xa/members/op2dSa2Sl/secret/xIX1S91cCkKs/your-own-height');
        findYourHeightInput().should('have.value', '172');
    });

    specify('when I provide the wrong secret, then I can not submit my height', () => {
        // given
        cy.then(async () => {
            const group = await createGroup().withId('cvd21iIQsd').save();
            await createMember().withId('op2dSa2Sl').withGroup(group).save();
        });

        // when
        cy.visit('http://localhost:3000/groups/cvd21iIQsd/members/op2dSa2Sl/secret/xIX1S91cCkKs/your-own-height');

        // then
        cy.contains('Try starting again from your secret URL').should('exist');
    });

    function findYourHeightInput() {
        return cy.contains('label', 'Your height').click().focused();
    }

    function whenISubmitMyHeight(height: string) {
        findYourHeightInput().clear().type(height);
        cy.contains('button', 'Submit').click();
    }
})
