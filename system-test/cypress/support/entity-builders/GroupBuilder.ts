import {persistGroup} from 'all-torso-no-trousers-backend/src/database/repositories/group-repository';

export function createGroup() {
    return new GroupInProgress();
}

class GroupInProgress {

    private id = 'dd19aBzae';
    private name = 'support team';
    private secret = 'pw2p+48cs1QWz';
    private resultsReleasedEarly = false;

    public withId = (id: string) => {
        this.id = id;
        return this;
    }

    public withSecret = (secret: string) => {
        this.secret = secret;
        return this;
    }

    public withResultsReleasedEarly = () => {
        this.resultsReleasedEarly = true;
        return this;

    }

    public save = async () => {
        const group = {
            Id: this.id,
            Secret: this.secret,
            Name: this.name,
            ResultsReleasedEarly: this.resultsReleasedEarly
        };
        await persistGroup(group);
        return group;
    };
}