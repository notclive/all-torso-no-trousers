import {persistMember} from 'all-torso-no-trousers-backend/src/database/repositories/member-repository';

export function createMember() {
    return new MemberInProgress();
}

class MemberInProgress {

    private id = '6cbccBz21';
    private secret = 'LP2l982sSd2d';
    private groupId = 'dd19aBzae';
    private name = 'Charlie';
    private heightInCm: number;
    private memberIdsFromTallestToShortest: string[];

    public withId = (id: string) => {
        this.id = id;
        return this;
    }

    public withSecret = (secret: string) => {
        this.secret = secret;
        return this;
    }

    public withGroup(group: {Id: string}) {
        this.groupId = group.Id;
        return this;
    }

    public withName(name: string) {
        this.name = name;
        return this;
    }

    public withHeight(heightInCm: number) {
        this.heightInCm = heightInCm;
        return this;
    }

    public withGuess(guess: string[]) {
        this.memberIdsFromTallestToShortest = guess;
        return this;
    }

    public save = async () => {
        const member = {
            Id: this.id,
            Secret: this.secret,
            GroupId: this.groupId,
            Name: this.name,
            HeightInCm: this.heightInCm,
            MemberIdsFromTallestToShortest: this.memberIdsFromTallestToShortest
        };
        await persistMember(member);
        return member;
    };
}