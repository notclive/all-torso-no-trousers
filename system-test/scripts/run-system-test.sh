#!/bin/sh

# Kill all child processes on exit.
trap "kill 0" EXIT
# Exit if any command fails.
set -e

# Ensure logs directory exists.
mkdir -p logs

echo "Starting database..."
docker-compose up --force-recreate --detach

echo "Starting frontend..."
cd ../frontend
BROWSER=false yarn start > ../system-test/logs/frontend.log &

echo "Starting backend..."
cd ../backend
yarn start > ../system-test/logs/backend.log &

# Ensure database is listening.
sleep 10

echo "Creating database tables..."
cd ../system-test
./scripts/create-database-tables.sh

echo "Running cypress tests..."
yarn run cypress open
