#!/bin/sh

# Exit when any command fails.
set -e

echo "Creating Member table"

aws dynamodb create-table \
    --table-name Member \
    --attribute-definitions AttributeName=GroupId,AttributeType=S AttributeName=Id,AttributeType=S \
    --key-schema AttributeName=GroupId,KeyType=HASH AttributeName=Id,KeyType=RANGE \
    --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
    --endpoint-url http://localhost:8000 > /dev/null

echo "Creating Group table"

aws dynamodb create-table \
    --table-name Group \
    --attribute-definitions AttributeName=Id,AttributeType=S \
    --key-schema AttributeName=Id,KeyType=HASH \
    --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 \
    --endpoint-url http://localhost:8000 > /dev/null
